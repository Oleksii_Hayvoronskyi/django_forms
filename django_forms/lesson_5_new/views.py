from django.shortcuts import render

from django.http import HttpResponse
from . forms import MyForm


# def my_form(request):
#                              # as_p() - поверне форми як параграфи (з абзацу).
#     return HttpResponse(MyForm().as_p())


def my_form(request):
    form = MyForm(request.POST or None)
    if form.is_valid():
        print(form.cleaned_data)
        print(form.errors)
    else:
        #print(form.cleaned_data)
        print(form.errors)
    return render(request, 'form_page.html', context={'form': form})
