from django import forms
from durationwidget.widgets import TimeDurationWidget


class MyForm(forms.Form):
                                       # disabled=True - робить поле неактивним
    # name = forms.CharField(label='User name', disabled=True,
    #                        error_messages={'required': 'Please enter '
    #                                                    'available name'})
    # email = forms.EmailField(error_messages={'required': 'Please enter '
    #                                                      'available email'})
    # password = forms.CharField(max_length=10, min_length=5,
    #                            widget=forms.PasswordInput())
    #             # Якщо required=False, то це поле не обов’язкове до заповнення.
    # age = forms.IntegerField(required=False, help_text='Enter your current age')
    # agreement = forms.BooleanField(required=False)
    #                     # initial=10.1 - за замовченням відображатиметься 10.1
    # average_score = forms.FloatField(initial=10.1)
    birthday = forms.DateField(widget=forms.SelectDateWidget)
    work_experience = forms.DurationField(widget=TimeDurationWidget())
                                       # Зазначаємо у вигляді кортежа.
    gender = forms.ChoiceField(choices=[('1', 'man'), ('2', 'woman'),
                                        ('3', 'unknown')])
