from django.apps import AppConfig


class Lesson5NewConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'lesson_5_new'
