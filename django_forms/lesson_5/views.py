from django.shortcuts import render
from django.http import HttpResponse
from . import forms


def form(request):
    form_for_defender = forms.DefenderOneForm
    form_for_award = forms.AwardForm
    form_contact = forms.ContactForm
    contex = {
        'form_for_defender': form_for_defender,
        'form_for_award': form_for_award,
        'form_contact': form_contact
    }
    return render(request, 'form.html', contex)


def defender_add(request):
    form = forms.DefenderOneForm(request.POST)
    result = 'Захисника усіпшно додано %s' %request.path
    if request.method == 'POST':
        if form.is_valid():
            data = form.cleaned_data
            form.save()
            print(data)
            return HttpResponse('Захисника додано! %s' %request.path)


def award_add(request):
    form = forms.AwardForm(request.POST)
    if request.method == 'POST' and form.is_valid():
        data = form.cleaned_data
        form = form.save()
        return HttpResponse('Нагороду додано!')
