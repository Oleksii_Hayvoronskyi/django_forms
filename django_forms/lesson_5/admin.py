from django.contrib import admin
from . import models

admin.site.register(models.Defender)
admin.site.register(models.Award)