from django.forms import ModelForm, Form
from . import models
from django import forms
from .models import Defender
from .models import Award
from django.http import HttpResponse


class DefenderOneForm(ModelForm):
    class Meta:
        model = Defender
        fields = ['name', 'submission', 'city']


class AwardForm(ModelForm):
    class Meta:
        model = Award
        fields = ['defender', 'award', 'description']


class ContactForm(forms.Form):
    boolean_field = forms.NullBooleanField()
    float_field = forms.FloatField()
    name_sender = forms.CharField(max_length=100, label='Введіть Ваше ім’я')
    message = forms.CharField(widget=forms.Textarea, label='Повідомлення')
    sender = forms.EmailField(label='Введіть Ваш емейл')
