from django.urls import path
from . import views



urlpatterns = [
    path('', views.form),
    path('add-defender/', views.defender_add),
    path('add-award/', views.award_add),

]

