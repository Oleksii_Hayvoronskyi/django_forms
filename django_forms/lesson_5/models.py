from django.db import models


# Клас захисників українських міст.
class Defender(models.Model):
    CHOICES_FOR_CITY=(
        ('kyiv', 'Київ'),
        ('donetsk', 'Донецьк'),
        ('mariupol', 'Маріуполь'),
        ('kherson', 'Херсон'),
    )
    name = models.CharField(
        max_length=200, verbose_name='Позивний захисника'
    )
    submission = models.CharField(
        max_length=200, verbose_name='Кому підпорядковується'
    )
    city = models.CharField(
        choices=CHOICES_FOR_CITY, max_length=200, verbose_name='Місто',
        help_text='Оберіть місто зі списку'
    )

    def __str__(self):
        return 'Ім’я %s' % self.name


# Клас нагород
class Award(models.Model):
    defender = models.ForeignKey(
        Defender, verbose_name='Власник нагороди', on_delete=models.CASCADE
    )
    award = models.CharField(
        max_length=150, verbose_name='Нагорода'
    )
    description = models.TextField(
        max_length=200, verbose_name='За які заслуги отримано нагороду'
    )

    def __str__(self):
        return self.award
